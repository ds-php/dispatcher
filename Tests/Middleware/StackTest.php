<?php

namespace Tests\Middleware;

use Ds\Middleware\Stack;

/**
 * Class StackTest
 * @package Tests\Middleware
 */
class StackTest extends \PHPUnit\Framework\TestCase
{

    public $request;
    public $response;
    public $stack;
    public $collector;
    public $container;

    public $objects = [];


    protected function setUp() : void
    {
        $this->container = $this->getMockBuilder('Interop\Container\ContainerInterface')->getMock();
        $this->request = $this->getMockBuilder('Psr\Http\Message\RequestInterface')->getMock();
        $this->response = $this->getMockBuilder('Psr\Http\Message\ResponseInterface')->getMock();
        $this->stack = new Stack();
    }
    
    public function testGetNamespaces()
    {
        $expected = [];
        $actual = $this->stack->getNamespaces();
        $this->assertEquals($expected, $actual);
    }

    public function testWithNamespace()
    {
        $newStack = $this->stack->withNamespace('my-new-namespace');
        $expected = [
            'my-new-namespace'
        ];
        $actual = $newStack->getNamespaces();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Private add method.
     */
    public function testAddCallable()
    {
        $middleware = function(){
            return true;
        };

        $this->stack = $this->stack->withMiddleware(
            $middleware, 
        );

        $reflector = new \ReflectionClass("\Ds\Middleware\Stack");
        $prop = $reflector->getProperty("_middleware");
        $prop->setAccessible(true);
        $stacks = $prop->getValue($this->stack);
        $stack = $stacks['middleware'];
        $this->assertEquals("callable", $stack[0]["type"]);
        $this->assertEquals(true, $stack[0]["callable"]());
    }
}
