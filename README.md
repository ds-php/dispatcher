# Middleware

Middleware can either be:
 * an instance of `Ds\Middleware\MiddlewareInterface
 * Extend `Ds/Middleware/Middleware
 * Closure with the following signature:

```
    function(ServerRequest $request, ResponseInterface $response, callable $next = null){

         //code before calling next middleware.
         if ($next){
            $next($request, $response)
         }

         //code after calling next middleware.
         return $response;
    }

```

# Stacks

Stacks are named collections of middlewares

```
    $stack = new Ds\Middleware\Stack();
    $stack->withNamespace('My\Class\Namespace');    
    $stack->withMiddleware('MyClass::MyMethod', 'stackId', ['stackName'])
    $stack->withMiddleware(function(
        RequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) : ResponseInterface {
        
        //do stuff before other middleware are called
        
        if ($next) {
            $response = $next($request, $response);
        }

        //do stuff after
        return $response;

    }, 'stackId', ['stackName','altName'])

```

# Pipe

Pipes execute Stacks and return a PSR ResponseInterface

```
    $pipe = new Ds\Middleware\Pipe();
    $pipe = $pipe->withContainer($container)

    $middlewareQueue = $pipe->fromStack($stack, 'stackId', 'stackName');

    //call using invoke
    $response = $middlewareQueue($request, $response);

    //call using method 'execute'
    $response = $middlewareQueue->execute($reqest, $response);

```