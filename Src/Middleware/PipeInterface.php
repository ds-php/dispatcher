<?php

namespace Ds\Middleware;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class PipeInterface
 *
 * @package Ds\Middleware
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://github.com/djsmithme/middleware
 */
interface PipeInterface
{
    /**
     * Add ContainerInterface to pipe.
     *
     * @param ContainerInterface $container
     * @return PipeInterface
     */
    public function withContainer(ContainerInterface $container);

    /**
     * Returns Container
     * @return ContainerInterface
     */
    public function getContainer();

    /**
     * Get named queue from StackInterface.
     *
     * @param StackInterface $stack
     * @param string $name
     * @param string $routeName
     * @return PipeInterface
     */
    public function fromStack(StackInterface $stack, $name, $routeName);

    /**
     * Call Middleware from queue.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function execute(RequestInterface $request, ResponseInterface $response);

    /**
     * Removes last item from the prepared queue if present.
     *
     * @return array
     * @throws \OutOfBoundsException
     */
    public function deque();

    /**
     * @return RequestInterface
     */
    public function getRequest();

    /**
     * @return ResponseInterface
     */
    public function getResponse();

    /**
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response);
}
