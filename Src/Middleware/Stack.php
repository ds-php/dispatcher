<?php

namespace Ds\Middleware;

use Ds\Middleware\Exceptions\StackException;

/**
 * Middleware Stack.
 *
 * Middlewares are added to stack using Stack::with.
 * Stacks can be fetched using Stack::getStack
 * Stacks can be executed using Pipe::fromStack::execute
 *
 * @package Ds\Middleware
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://github.com/djsmithme/middleware
 */
class Stack implements StackInterface
{
    /**
     * Middleware Collection.
     *
     * @var array
     */
    private $_middleware = [];
    /**
     * Collection of Namespaces.
     *
     * @var array $_namespaces
     */
    private $_namespaces = [];

    /**
     * Add Namespace to Stack.
     *
     * @param string $namespace Middleware namespace.
     *
     * @return StackInterface
     */
    public function withNamespace($namespace): StackInterface
    {
        $new = clone $this;
        $new->_namespaces[] = $namespace;
        return $new;
    }

    /**
     * Return added Namespaces.
     *
     * @return array
     */
    public function getNamespaces(): array
    {
        return $this->_namespaces;
    }

    /**
     * Add Middleware to Collection.
     * Middleware can be either a class, string or callback.
     *
     * @param string|object|callable $middleware Classname, Class Object or Closure.
     * @param string $stack Stack name.
     * @param array $route Route Names.
     *
     * @return StackInterface
     *
     * @throws StackException
     */
    public function withMiddleware(
        $middleware,
        $stack = 'middleware',
        array $route = ['global']
    ): StackInterface
    {
        switch ($middleware) {
            case \is_string($middleware):

                if (!\class_exists($middleware)) {
                    $resolved = $this->resolveClass($middleware);
                    if ($resolved) {
                        return $this->_addToStack(
                            $stack, 'class', $resolved, $route
                        );
                    }
                } else {
                    return $this->_addToStack($stack, 'class', $middleware, $route);
                }
                break;
            case \is_callable($middleware):
                return $this->_addToStack($stack, 'callable', $middleware, $route);
                break;
            case \is_object($middleware):
                return $this->_addToStack($stack, 'object', $middleware, $route);
                break;
        }
        throw new StackException(
            'Middleware does not exist, is not a class or is not callable.'
        );
    }

    /**
     * @param $middleware
     * @return bool|string
     */
    public function resolveClass($middleware)
    {
        foreach ($this->_namespaces as $namespace) {
            if (class_exists($namespace . $middleware)) {
                return $namespace.$middleware;
            }
        }
        return false;
    }

    /**
     * Internal function to add parsed middleware to stack.
     *
     * @param string $stackName Name of middleware stack.
     * @param string $type Middleware type.
     * @param string|callable $middleware Middlware
     * @param array $routeName Route names to attach middleware to.
     *
     * @internal
     *
     * @return StackInterface
     */
    private function _addToStack($stackName, $type, $middleware, array $routeName): StackInterface
    {
        $new = clone $this;

        $new->_middleware[$stackName][] = [
            'routeName' => $routeName,
            'callable' => $middleware,
            'type' => $type
        ];

        return $new;
    }

    /**
     * Return Middleware Stack.
     *
     * @param string $stackName Name of stack.
     * @param array $routeNames Route names.
     *
     * @return array
     */
    public function getStack($stackName, $routeNames = ['global']): array
    {
        $stack = [];

        if (!isset($this->_middleware[$stackName])) {
            return $stack;
        }

        foreach ($this->_middleware[$stackName] as $key => $middleware) {
            foreach ($middleware['routeName'] as $routeName) {
                if (in_array($routeName, $routeNames)) {
                    $stack[$key] = $middleware;
                }
            }
        }

        return array_reverse($stack);
    }
}
