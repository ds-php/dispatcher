<?php

namespace Ds\Middleware;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface MiddlewareInterface
 *
 * @package Rs\Middleware
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://github.com/djsmithme/middleware
 */
interface MiddlewareInterface
{
    /**
     * MiddlewareInterface constructor.
     *
     * @param ContainerInterface $container DI Container
     */
    public function __construct(ContainerInterface $container);

    /**
     * Invoke Middleware
     *
     * @param RequestInterface $request Server Response
     * @param ResponseInterface $response Server Request
     * @param callable|null $next Next Middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(
        RequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    );
}
