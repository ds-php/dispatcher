<?php

namespace Ds\Middleware;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Ds\Middleware\Exceptions\QueueException;

/**
 * Pipe is used to execute Stacks and return a ResponseInterface.
 *
 * @package Ds\Middleware
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://github.com/djsmithme/middleware
 */
class Pipe implements PipeInterface
{

    /**
     * @var array Active Middleware Stack
     */
    protected $queue = [];

    /**
     * @var ContainerInterface  DI Container
     */
    protected $container;

    /**
     * @var RequestInterface Server Request
     */
    protected $request;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * Add ContainerInterface to pipe.
     *
     * @param ContainerInterface $container DI Container
     *
     * @return PipeInterface
     */
    public function withContainer(ContainerInterface $container)
    {
        $new = clone $this;
        $new->container = $container;
        return $new;
    }

    /**
     * Returns Container
     *
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }


    /**
     * Get named queue from StackInterface.
     *
     * @param StackInterface $stack Middleware Stack.
     * @param string $name Name of middleware stack.
     * @param string $routeName Route names to be executed
     *
     * @return PipeInterface
     */
    public function fromStack(StackInterface $stack, $name, $routeName)
    {
        $new = clone $this;
        $new->queue = $stack->getStack($name, $routeName);
        return $new;
    }

    /**
     * Get Request.
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Get Response.
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * With Request.
     * @param RequestInterface $request
     * @return RequestInterface
     *
     */
    public function withRequest(RequestInterface $request){
        $this->request = $request;
    }

    /**
     * With Response.
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function withResponse(ResponseInterface $response){
        $this->response = $response;
    }

    /**
     * Called from $next() middleware callable.
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response)
    {
        return $this->execute($request, $response);
    }

    /**
     * Call Middleware from queue.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function execute(RequestInterface $request, ResponseInterface $response)
    {
        $this->request = $request;

        $this->response = $response;

        try {
            $middleware = $this->deque();

            switch ($middleware['type']) {
                case 'callable':
                    return $middleware['callable']($request, $response, $this);
                case 'class':
                    $callMid = new $middleware['callable']($this->container);
                    return $callMid($request, $response, $this);
            }
        } catch (QueueException $oe) {
            $this->response = $response;
        }

        return $this->response;
    }

    /**
     * Removes last item from the prepared queue if present.
     *
     * @return array
     * @throws QueueException
     */
    public function deque()
    {
        if (empty($this->queue)) {
            throw new QueueException('Stack empty.');
        }

        return array_pop($this->queue);
    }
}
